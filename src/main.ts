import { PORT } from './config/env.config';
import { Application, Request, Response, NextFunction } from 'express';
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import helmet from 'helmet';

const app: Application = express();
require('./db/config.db');

const FRONTEND_SERVER = process.env.FRONTEND_SERVER || 'localhost';

var corsOptions = {
  origin: 'http://' + FRONTEND_SERVER
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// securing Express apps - info: https://github.com/helmetjs/helmet
app.use(helmet());

// example of GET
app.get('/', (req: Request, res: Response) => {
  res.json({ message: 'This is Bitcoin Analyzer backend server.' });
});

// ADD BUSINESS LOGIC ROUTES HERE
app.use('/api/tradeHistory', require('./routes/trade-history.route'));

// error handling
// not found error
app.use(function(req: Request, res: Response, next: NextFunction) {
  let err = new Error('Not Found');
  // err.status = 404;
  next(err);
});

// unauthorized error
app.use(function (err: Error, req: Request, res: Response, next: NextFunction) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({"message" : err.name + ": " + err.message});
  }
});

// development error: print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err: Error, req: Request, res: Response, next: NextFunction) {
      // res.status(err.status || 500);
      res.render('error', {
          message: err.message,
          error: err
      });
  });
}

// production error: no stacktrace
app.use(function(err: Error, req: Request, res: Response, next: NextFunction) {
  // res.status(err.status || 500);
  res.render('error', {
      message: err.message,
      error: {}
  });
});


// set port, listen for requests
app.listen(PORT, () => {
  console.log(`Bitcoin Analyzer server is running on port ${PORT}.`);
});



/**
 * Core of the program
 */
import { startProgram } from './core/main.core';
startProgram();