import * as path from 'path';
import * as dotenv from "dotenv";
dotenv.config({ path: path.join(__dirname, '../../', '.env') });

export const NODE_ENV = process.env.NODE_ENV;
export const PORT = process.env.PORT;
export const DB_CONFIG = process.env.DB_CONFIG;

export const log = require(path.join(__dirname, '../logging', 'logging'));
log.setIsDebugMode(true);