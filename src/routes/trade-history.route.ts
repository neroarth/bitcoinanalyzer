const trade_history_router = require('express').Router();
const tradeController = require("../controllers/trade-history.controller");

// GET BACK ALL THE BENCHES
trade_history_router.get('/getAllTrades', tradeController.getAllTrades);

// EXPORT
module.exports = trade_history_router;