/*
 * This file contains constants that are grouped as enums.
 * The main purpose of this file is to prevent typo errors.
 * Instead of retyping the same string over and over,
 * the developer can just simply reference the string using these enums.
 */

export const enum SystemFilePath {
    appDataPath = 'BitcoinAnalyzer',
    logPath = 'Logs'
}

export const enum LoggingLevel {
    info = 'info',
    warn = 'warn',
    error = 'error'
}

export const enum GetTradeEnum {
    interval = 15 * 60 * 1000 // 15 min
}