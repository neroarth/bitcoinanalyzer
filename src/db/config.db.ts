const mongoose = require('mongoose');
import { DB_CONFIG } from '../config/env.config';
import * as path from 'path';
import { LoggingLevel } from '../enums/enums';
const log = require(path.join(__dirname, '../logging', 'logging'));

mongoose.connect(DB_CONFIG, { useNewUrlParser: true, useUnifiedTopology: true,  useFindAndModify: false }, (err: any) => {
    if(err) {
        log.logToFile(LoggingLevel.error, 'Error in connecting Bitcoin Analyzer MongoDB server...');
        return;
    }

    log.logToFile(LoggingLevel.info, 'Connected to Bitcoin Analyzer MongoDB server....');
});