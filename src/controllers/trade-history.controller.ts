import { Error } from '../models/class/error.model';
import { Result } from '../models/class/result.model';
import { TradeHistory } from '../models/class/trade-history.model';
import { LoggingLevel } from '../enums/enums';
import { log } from '../config/env.config';

import express from 'express';

const _tradeHistoryService = require('../services/trade-history.service');

/**
 * Get request - Get whole trade history
 * @remarks
 * 1) get all trade history
 * 
 * @param
 * No parameters is needed
 *
 * @returns Result class (+array of TradeHistory)
 */
function getAllTrades (req: express.Request, res: express.Response) {
    log.logToFileForDebug(LoggingLevel.info, "Debug -- getAllTrades");

    try{
        _tradeHistoryService.findAllTrades(function (err: Error, result: TradeHistory[]) {
            if (err) {
                log.logToFile(LoggingLevel.error, "normal error occurred in get All Trades: ");
                log.logToFile(LoggingLevel.error, err);
                res.send( new Result(err) );
                return;
            }
            return res.send( new Result(null, result) );
        });

    } catch (err) {
        log.logToFile(LoggingLevel.error, "unexpected error occurred in get All Trades: ");
        log.logToFile(LoggingLevel.error, err);
        return res.send( new Result(new Error(9001)));
    }
}

module.exports = {
    getAllTrades: getAllTrades
}