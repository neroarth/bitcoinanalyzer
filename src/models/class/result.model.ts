import { Error } from './error.model';

export class Result {
    error: Error;
    returnedObj: Object;

    constructor(error: Error = null, returnedObj: Object = null) {
        this.error = error;
        this.returnedObj = returnedObj;
    }
}
