export class TradeHistory {
    _id = 0; // is actually timestamp
    usd = 0;
    myr = 0;
    gmt8 = '';

    constructor(_id: number, usd: number, myr: number, gmt8: string) {
        this._id = _id;
        this.usd = usd;
        this.myr = myr;
        this.gmt8 = gmt8;
    }
}
