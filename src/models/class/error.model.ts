import { LoggingLevel } from '../../enums/enums';

import * as path from 'path';
import { log } from '../../config/env.config';

export class Error {
    code = 0;
    desc = '';
    suggestedAction = '';
    isTimeout = false;

    constructor(code: number, isTimeout = false) {
        this.code = code;
        this.getErrDetails(code);
        this.isTimeout = isTimeout;
    }

    /*
        1000 - 1999: db error
        9000 - 9999: unexpected error
    */
    getErrDetails(code: number){
        switch (code) {
            case 0:
                this.desc = '';
                this.suggestedAction = '';
                break; 
            case 1000:
                this.desc = 'Failed to save trade history';
                this.suggestedAction = '';
                break; 
            case 1001:
                this.desc = 'Failed to get all trade history';
                this.suggestedAction = '';
                break; 
            case 1002:
                this.desc = 'Failed to get trade history by ID/timestamp';
                this.suggestedAction = '';
                break; 
            case 9001:
                this.desc = 'An unexpected error happened';
                this.suggestedAction = '';
                break; 
            default:
                this.desc = 'Unknown error code returned';
                this.suggestedAction = 'Please contact support';
                log.logToFile(LoggingLevel.error, 'unknown error code returned: ' + code);
                break;
        }
    }
}
