/**
 * Model for bench collection
 * 
 * _id - trade id = timestamp in UTC
 * usd - USD value of 1 btc
 * myr - MYR value of 1 btc
 * 
 */
const TradeHistorySchema = require('mongoose').Schema({
    _id: {
        type: Number,
        required: true
    },
    usd: {
        type: Number,
        required: true
    },
    myr: {
        type: Number,
        required: true
    },
    gmt8: {
        type: String,
        required: true
    }

}, {
    versionKey: false
});

module.exports = require('mongoose').model('trade_history', TradeHistorySchema, 'trade_history');