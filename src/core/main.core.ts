import { log } from '../config/env.config';
import { TradeHistory } from '../models/class/trade-history.model';
import { LoggingLevel, GetTradeEnum } from '../enums/enums';
import axios, { AxiosError, AxiosResponse } from 'axios';
import moment from 'moment';

const _tradeHistoryService = require('../services/trade-history.service');
const twilio = require('twilio');

const alertIfLessThanInUsdArr = new Array(
    8500,
    8600,
    8700,
    8800,
    8900,
    9000,
    9100,
    9200
);

const alertIfMoreThanInUsdArr = new Array(
    9600,
    9700,
    9800,
    9900,
    10000,
    10100,
    10200,
    10300
);

const dndStartTime = 2330;
const dndEndTime = 800;

/**
 * link to get trade price https://api.coinbase.com/v2/exchange-rates
 */
export function startProgram() {
    // log.logToFile(LoggingLevel.info, 'Start core program');
    getCurrentBtcPrice();
    setInterval( () => {
        getCurrentBtcPrice();
    }, GetTradeEnum.interval);
}

// TODO: get open and close price so that caan calculate rate up and down, store outside
function getCurrentBtcPrice() {
    axios.get('https://api.coinbase.com/v2/exchange-rates?currency=BTC')
    .then( (response: AxiosResponse) => {
        let usd = 0;
        let myr = 0;
        const timestamp = new Date().getTime();
        const gmt8 = moment(new Date(timestamp)).format('DD/MM/YYYY HH:mm:ss');

        const data = response.data.data;
        if (data.hasOwnProperty('rates')) {
            if (data['rates'].hasOwnProperty('USD')) {
                usd = data['rates']['USD'] as number;
                usd = convertToTwoDecimal(usd);
                
            }
            if (data['rates'].hasOwnProperty('MYR')) {
                myr = data['rates']['MYR'] as number;
                myr = convertToTwoDecimal(myr);
            }
        }
        if (usd > 0 && myr > 0) {
            let trade = new TradeHistory(timestamp, usd, myr, gmt8);
            _tradeHistoryService.createTradeHistory(trade, (err: Error, result: Object) => {
                if (err) {
                    log.logToFile(LoggingLevel.error, 'Failed to save trade data');
                    log.logToFile(LoggingLevel.error, err);
                    return;
                }
                log.logToFile(LoggingLevel.info, 'USD: ' + numberWithCommas(usd) + '  |_|  MYR: ' + numberWithCommas(myr));
            });
        }

        const alertMsg = '\n\n*USD = ' + numberWithCommas(usd) + '*\n*MYR = ' + numberWithCommas(myr) + '*';

        for (let i = 0; i < alertIfLessThanInUsdArr.length; i++) {
            const alertPriceInUsd = alertIfLessThanInUsdArr[i] as number;
            if (usd > 0 && usd <= alertPriceInUsd) {
                const tempMsg = 'ALERT: price is BELOW\n*USD ' + numberWithCommas(alertPriceInUsd) + '*' + alertMsg;
                sendWhatsAppMessage(tempMsg);
                return;
            }
        }

        for (let i = 0; i < alertIfMoreThanInUsdArr.length; i++) {
            const alertPriceInUsd = alertIfMoreThanInUsdArr[i] as number;
            if (usd > 0 && usd >= alertPriceInUsd) {
                const tempMsg = 'ALERT price is ABOVE\n*USD ' + numberWithCommas(alertPriceInUsd) + '*' + alertMsg;
                sendWhatsAppMessage(tempMsg);
                return;
            }
        }
    })
    .catch( (err: AxiosError) => {
        log.logToFile(LoggingLevel.error, 'Failed to get trade details from coinbase');
        log.logToFile(LoggingLevel.error, err);
    });
}

function sendWhatsAppMessage(msgToSend: string) {
    log.logToFile(LoggingLevel.info, 'Send whatsapp message');

    let date = new Date();
    const currentTime: number = parseInt(date.getHours().toString() + date.getMinutes().toString());

    // if current time is inside the DND time range, don't send whatsapp message, else send
    if (currentTime < dndEndTime || currentTime > dndStartTime) {
        return;
    }

    let accountSid = 'AC0746dd9bd66a1ecfac7097fdaa341f06'; // Your Account SID from www.twilio.com/console
    let authToken = 'b82599f3e84f48b4796c5b5cf516f21e';   // Your Auth Token from www.twilio.com/console

    let client = new twilio(accountSid, authToken);

    client.messages.create({
        body: msgToSend,
        to: 'whatsapp:+60164763391',  // Text this number
        from: 'whatsapp:+14155238886' // From a valid Twilio number
    })
    .then((message: any) => {
        // console.log(message);
    })
    .catch( (err: any) => {
        log.logToFile(LoggingLevel.error, 'Failed to send whatsapp message with twilio');
        log.logToFile(LoggingLevel.error, err);
    });
}

// not working if decimal places more than 2 or 3 but not a problem here since it won't have that many decimals
// refer from here: https://stackoverflow.com/questions/2901102/how-to-print-a-number-with-commas-as-thousands-separators-in-javascript
function numberWithCommas(x: number) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

// convert to 2 decimal only
function convertToTwoDecimal(num: number) : number {
    return Math.round(num *100 ) / 100;
}

module.exports = {
    startProgram: startProgram
}