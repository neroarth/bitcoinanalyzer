
// import * as path from 'path';
import { SystemFilePath, LoggingLevel } from '../enums/enums';
// import getPath from 'platform-folders';

const log = require('electron-log');

// set log file path
// const logPath = path.join(getPath('appData') as string, SystemFilePath.appDataPath, SystemFilePath.logPath, 'Bitcoin Analyzer - eLab.log');
// log.transports.file.file = logPath;
// Set approximate maximum log size in bytes. When it exceeds,
// the archived log will be saved as the log.old.log file
// set size 5mb
log.transports.file.maxSize = 5 * 1024 * 1024;
// Set maximum log files count. When it exceeds,
// oldest old log file is deleted.

let _isDebugMode = false;

function setIsDebugMode(isDebugMode: boolean) {
  _isDebugMode = isDebugMode;
}

function logToFile(level: LoggingLevel, message: string) {
    switch(level) {
        case LoggingLevel.info:
            log.info(message);
          break;
        case LoggingLevel.warn:
            log.warn(message);
          break;
        case LoggingLevel.error:
            log.error(message);
          break;
        default:
            log.info(message);
    }
}

// On release, this log should not do anything, this is for logging things like instrument data, update progress, etc.
function logToFileForDebug(level: LoggingLevel, message: string) {
  if (!_isDebugMode) {
    return;
  }

  switch(level) {
      case LoggingLevel.info:
          log.info(message);
        break;
      case LoggingLevel.warn:
          log.warn(message);
        break;
      case LoggingLevel.error:
          log.error(message);
        break;
      default:
          log.info(message);
  }
}

module.exports = {
  setIsDebugMode: setIsDebugMode,
  logToFile: logToFile,
  logToFileForDebug: logToFileForDebug
};
