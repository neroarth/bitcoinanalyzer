// Database api (bench collection)

export {}
import { Error } from '../models/class/error.model';
import { TradeHistory } from '../models/class/trade-history.model';
import * as path from 'path';
import { LoggingLevel } from '../enums/enums';

const log = require(path.join(__dirname, '../logging', 'logging'));
const tradeHistoryDb = require('../models/db/trade-history.model');

/**
 * records a trade price history
 * 
 * @param {TradeHistory} trade history - new trades
 */
async function createTradeHistory(tradeHistory: TradeHistory, onFinish: Function){
    try{
        const th = new tradeHistoryDb (tradeHistory);
        const savedTh = await th.save();
        onFinish(null, savedTh);
    } catch (err) {
        log.logToFile(LoggingLevel.error, 'Errors in saving trade history: ' + err);
        onFinish(new Error(1000), null);
    }
};

/**
 * Get all trades
 * 
 * @returns trade history class
 */
async function findAllTrades(onFinish: Function){
    try {
        const data = await tradeHistoryDb.find();
        onFinish(null, data);
    } catch (err) {
        log.logToFile(LoggingLevel.error, 'Errors in finding all trades: ' + err);
        onFinish(new Error(1001), null);
    }
}

/**
 * Get a trade by id/timestamp
 * 
 * @param {string} id - trade id / timestamp
 * @returns trade history class
 */
async function findTradeById(id: number, onFinish: Function) {
    try {
        const data = await tradeHistoryDb.findOne({ _id: id });
        onFinish(null, data);
    } catch (err) {
        log.logToFile(LoggingLevel.error, 'Errors in finding trade history by id: ' + err);
        onFinish(new Error(1002), null);
    }
}

module.exports = {
    createTradeHistory: createTradeHistory,
    findAllTrades: findAllTrades,
    findTradeById: findTradeById
}